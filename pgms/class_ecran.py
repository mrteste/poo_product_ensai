# -*- coding: utf-8 -*-
"""
Created on Sat May 11 23:32 2019

@author: id1229
"""

import collections
#v36_perso
from pyautogui import typewrite
#v35_perso

class Ecran:
    """
    
    Classe abstraite - classe mère des différents types d'écran utilisés
    dans l'application (Menu, Prompt, Modif).
    
    """
    def __init__(self):
        pass
        

class Menu(Ecran):
    """
    
    Les menus sont les écrans principaux qui offrent à l'utilisateur plusieurs
    choix possibles. L'utilisateur est alors orienté grâce aux paramètres 
    type_suivants et suivants qui décrivent les écrans ou actions qui suivent.
    
    """
    def __init__(self, numeros, intitules, titre, type_suivants, suivants):
        """
        :param numeros: numéro de chaque choix mis à disposition de l'utilisateur
        :type numeros: list
        :param intitules: libellés en clair propres à chaque numéro du menu
        :type intitules: list
        :param titre: titre du menu
        :type titre: str
        :param type_suivants: types d'écran après validation (cela peut aussi 
        être une fonction qui est exécutée et qui renvoie au même écran ensuite)
        :type type_suivants: list
        :param suivants: numéro de l'écran affiché après validation
        Si le type_suivant est une fonction, ici sera renseigné le code qui devra 
        s'exécuter.
        :type suivant: list
        """
        self.entrees = collections.OrderedDict()
        self.titre = titre
        self.suivants = dict()
        self.type_suivants = dict()
        for num in range(len(numeros)):
            self.entrees[numeros[num]] = intitules[num]
            self.suivants[numeros[num]] = suivants[num]
            self.type_suivants[numeros[num]] = type_suivants[num]
    
    
    def getTypeSuivants(self):
        """
        
        Permet de récupérer le type des écrans suivant le menu actuel
        
        """
        return self.type_suivants
    
    def getSuivants(self):
        """
        
        Permet de récupérer les numéros des écrans suivant le menu actuel
        
        """
        return self.suivants
    
    def getNext(self, choix):
        """
        
        Renvoie, en fonction du choix de l'utilisateur, l'écran suivant.
        Si le choix ne correspond pas à un choix possible, la méthode renvoie
        False.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param choix: texte entré par l'utilisateur
        :type choix: str
        :return: écran suivant l'écran actuellement affiché
        :rtype: str ou bool
        """
        return self.suivants[choix] if choix in self.suivants.keys() else False
    
    def afficher(self):
        """
        Méthode qui gère l'affichage d'un écran de type Menu. Elle renvoie la 
        valeur entrée par l'utilisateur dans la zone de texte qui s'affiche
        en-dessous du menu pour qu'il décrive son choix.
        
        - **retour** et **types de retour**::
        
        :return: le choix émis par l'utilisateur
        :rtype: str
        """
        
        longueur = 40
        for entree in self.entrees.values():
            longueur = len(entree) if len(entree) > longueur else longueur
        longueur += 10
        total = longueur + 4
        horiz = "-" * total
        vide = "| " + ' ' * longueur + " |"
        print(horiz)
        print("| " + self.titre.center(longueur) + " |")
        print(vide)
        for num, intitule in self.entrees.items():
            chaine = str(num) + " : " + intitule
            print("| " + chaine.ljust(longueur)+ " |")
        print(vide)
        print(horiz)
        return(input("\n"+"-"*50+"\nSaisissez le numéro de votre choix: "))


        
#class Prompt
class Prompt(Ecran):
    """
    Nous appelons Prompt dans notre application, un type d'affichage particulier
    qui associe un texte de présentation et un champ libre. A la différence d'un
    menu, le Prompt ne propose pas des alternatives parmi lesquelles choisir.
    L'utilisateur pourra être invité à renseigner un code produit ou un code
    utilisateur, un nom d'ingrédient, etc. En fonction de l'instance en question,
    des fonctions peuvent être exécutées avant, ou après.
    """
    def __init__(self, intitule, avant_prompt, avant_destination, type_suivant, suivant):
        """
        :param intitule: texte qui sera affiché à l'écran
        :type intitule: str
        :param avant_prompt: code exécuté avant l'affichage à l'écran
        :type avant_prompt: str
        :param avant_destination: code s'exécutant avant de poursuivre sur 
        l'écran suivant
        :type avant_destination: str
        :param type_suivant: type d'écran après validation
        :type type_suivant: str
        :param suivant: numéro de l'écran affiché après validation
        :type suivant: str
        """
        self.entree = intitule
        self.avant_prompt = avant_prompt
        self.avant_destination = avant_destination
        self.suivant = suivant
        self.type_suivant = type_suivant
    
    def getAvantPrompt(self):
        """
        Récupère la fonction à exécuter avant l'affichage
        
        - **retour** et **types de retour**:
        
        :return: code à exécuter
        :rtype: str
        
        """
        return self.avant_prompt
    
    def getAvantDestination(self):
        """
        Récupère la fonction à exécuter après validation du choix par
        l'utilisateur.
        
        - **retour** et **types de retour**:
        
        :return: code à exécuter
        :rtype: str
        
        """
        return self.avant_destination
    
    def getTypeSuivant(self):
        """
        Récupère le type d'écran qui devra être affiché après validation
        de la part de l'utilisateur.
        
        - **retour** et **types de retour**:
        
        :return: type de l'écran ('menu', 'prompt')
        :rtype: str
        
        """
        return self.type_suivant
    
    def getSuivant(self):
        """
        Récupère le numéro d'écran qui devra être affiché après validation
        de la part de l'utilisateur.
        
        - **retour** et **types de retour**:
        
        :return: numéro de l'écran
        :rtype: str
        
        """
        return self.suivant
    
    def afficher(self):
        """
        Méthode qui gère l'affichage d'un écran de type Prompt. Elle renvoie la 
        valeur entrée par l'utilisateur dans la zone de texte qui s'affiche
        en-dessous du texte affiché pour qu'il décrive son choix.
        
        - **retour** et **types de retour**::
        
        :return: le choix émis par l'utilisateur
        :rtype: str
        """
        longueur = len(self.entree)
        total = longueur + 4
        horiz = "-" * total
        vide = "| " + ' ' * longueur + " |"
        print(horiz)
        print(vide)
        print("| " + self.entree.ljust(longueur) + " |")
        print(vide)
        print(horiz)
        return(input())
  
    
class Modif(Ecran):
    """
    Les écrans de type Modif permettent de gérer le cas particulier des
    modifications des produits dans la base, en gérant notamment la modification
    de l'existant.
    """
    def __init__(self, champ, intitule, defaut, avant_destination, type_suivant, suivant):
        """
        :param champ: intitulé de la colonne de la base des produits que
        l'utilisateur souhaite modifier
        :type champ: str
        :param intitule: libellé en clair du champ
        :type intitule: str
        :param defaut: code permettant l'accès à la valeur actuelle du champ
        :type defaut: str
        :param avant_destination: code s'exécutant avant le retour au menu d'origine
        :type avant_destination: str
        :param type_suivant: type d'écran après validation
        :type type_suivant: str
        :param suivant: numéro de l'écran affiché après validation
        :type suivant: str
        """
        self.champ = champ
        self.entree = intitule
        self.defaut = defaut
        self.avant_destination = avant_destination
        self.suivant = suivant
        self.type_suivant = type_suivant
    
    def getChamp(self):
        """
        Récupère le champ à modifier.
        
        - **retours** et **types de retour**:
        
        :return: champ
        :rtype: str
        """
        return self.champ
    
    def getDefaut(self):
        """
        Récupère la fonction à exécuter pour récupérer la valeur actuelle 
        du champ que l'utilisateur s'apprête à modifier.
        
        - **retours** et **types de retour**:
        
        :return: code d'une fonction à exécuter
        :rtype: str
        """
        return self.defaut
        
    def getAvantDestination(self):
        """
        Récupère le code à exécuter après validation par l'utilisateur et
        avant d'afficher l'écran suivant. La fonction récupérée permet de modifier
        la base des produits.
        
        - **retours** et **types de retour**:
        
        :return: code d'une fonction à exécuter
        :rtype: str
        """
        return self.avant_destination
    
    def getTypeSuivant(self):
        """
        Récupère le type d'écran qui devra être affiché après validation
        de la part de l'utilisateur.
        
        - **retour** et **types de retour**:
        
        :return: type de l'écran ('menu', 'prompt')
        :rtype: str        
        """
        return self.type_suivant
    
    def getSuivant(self):
        """
        Récupère le numéro d'écran qui devra être affiché après validation
        de la part de l'utilisateur.
        
        - **retour** et **types de retour**:
        
        :return: numéro de l'écran
        :rtype: str        
        """
        return self.suivant
    
    def afficher(self, val_defaut):
        """
        Méthode qui gère l'affichage d'un écran de type Modif. Elle renvoie la 
        valeur entrée par l'utilisateur dans la zone de texte qui s'affiche
        en-dessous du texte affiché pour qu'il décrive son choix.
        
        - **retour** et **types de retour**::
        
        :return: le choix émis par l'utilisateur
        :rtype: str
        
        .. note:: La fonctionnalité qui permet d'afficher la valeur actuelle 
            du champ afin qu'il puisse être modifié interactivement est disponible 
            grâce à la fonction `pyautogui::typewrite`. Malheureusement, cette
            fonctionnalité n'est pas disponible sur les machines de l'Ensai.
            La version mise à disposition pour une utilisation à l'Ensai ne 
            comprend donc pas cette fonctionnalité. A la place, l'ancienne valeur
            n'est donc qu'affichée sans pouvoir être modifiée interactivement.
        """
        longueur = len(self.entree)
        total = longueur + 25
        horiz = "-" * total
        vide = "| " + ' ' * (total-4) + " |"
        print(horiz)
        print(vide)
        print("| Vous pouvez modifier " +  self.entree.ljust(longueur) + " |")
        print(vide)
        print(horiz)
        #v36_perso
        typewrite(str(val_defaut))
        #v35_ensai_debut
        # print("La valeur actuelle est \n" + str(val_defaut))
        # print(horiz)
        #v35_ensai_fin
        return(input())
    
