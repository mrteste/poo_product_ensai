# -*- coding: utf-8 -*-
"""
Created on Sat May 11 23:32 2019

@author: id1229
"""

import os
import sys
import collections
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

#v36_ensai
from pyautogui import typewrite

chem_pgms = os.getcwd()+'\\pgms'
chem_data = os.getcwd()+'\\data'
sys.path.append(chem_pgms)
sys.path.append(chem_data)

from class_ecran import *
from class_utilisateurs import *
from class_produit import *


class Application:
    """
    La classe Application permet de gérer le comportement de l'application\
    en gérant notamment le chargement des données, la succession des écrans ou\
    l'autorisation des accès. C'est elle qui coordonne l'ensemble des autres\
    classes du projet.
    
    
    - **attributs de la classe**, **types des attributs**::
    
    :param localisation: type et numéro de l'écran affiché
    :type localisation: list of 2 str
    :param base_produits: base des produits de l'application
    :type base_produits: BaseProduits
    :param base_utilisateurs: base des utilisateurs de l'application
    :type base_utilisateurs: BaseUtilisateurs
    :param base_menus: base des menus
    :type base_menus: dict(Menu)
    :param base_prompts: base des textes prompts
    :type base_prompts: dict(Prompt)
    :param base_modifs: base des modifs
    :type base_modifs: dict(Modif)
    :param navigateur: représente la personne connectée à l'application
    :type navigateur: Personne
    :param produit_choisi: représente le produit sélectionné par l'utilisateur un produit fictif est créé par défaut
    :type produit_choisi: Produit
      
    .. note:: Pour la localisation, deux informations sont nécessaires : 
        le premier prend les valeurs 'MENU' pour menu, 'PROMPT' pour un texte prompt ou 'MODIF' pour un écran de modification,
        le second est un numéro désignant le menu, le texte, ou la modif concernée.
    
    """
    
    message_choix_incorrect = "-"*35+"\nVotre choix n'est pas correct.\n"\
    + "-"*35 + "\nVeuillez recommencer :"
    
    def __init__(self, localisation = ['MENU','0'], base_produits = None, \
    base_utilisateurs = None, base_menus = None, base_prompts = None,\
    base_modifs = None, navigateur = Personne(), produit_choisi = Produit('0')):
        
        self.localisation = localisation
        self.base_produits = base_produits
        self.base_utilisateurs = base_utilisateurs
        self.base_menus = base_menus
        self.base_prompts = base_prompts
        self.base_modifs = base_menus
        self.navigateur = navigateur
        self.produit_choisi = produit_choisi
     
    
    def getLocalisation(self):
        return self.localisation
         
    def connexion(self):
        """
        La méthode connexion permet de gérer la connexion d'un utilisateur avec 
        un compte validé dans la base utilisateur.
        
        - **retour** et **types de retour**::

        :return: Renvoie True si la connexion est acceptée, False sinon 
        :rtype: bool
        """
        pseudo = input('-'*25 + '\nVotre pseudo :')
        mdp = input('-'*25 + '\nVotre mot de passe :')
        if self.base_utilisateurs.verificationSiDansBase(pseudo, mdp):
            if self.base_utilisateurs.dicoUtilisateurs[pseudo].getEtat() == 'V':
                return self.base_utilisateurs.dicoUtilisateurs[pseudo]
            else:
                print("-"*40+"\nVotre compte n'est pas encore validé.\
                \nVeuillez attendre la validation d'un administrateur.\n"\
                +"-"*40)
                return False
        else:
            print("-"*45+"\nVotre pseudo ou votre mot de passe est erroné.\n"\
            +"-"*45)
            return False                    
    
    def setNavigateur(self, navig):
        """
        Cette méthode permet modifier l'utilisateur en cours de connexion.
        
        - **paramètres**, **types**, **retour** et **types de retour**::

        :param navig: instance d'utilisateur
        :type navig: Personne, Contributeur ou Administrateur
        :return: L'intérêt de la méthode n'est pas dans son retour.
        :rtype: NoneType
          
        """
        self.navigateur = navig if navig else self.navigateur
        
    def setProduitChoisi(self, code):
        """
        Modifie l'attribut produit_choisi en fonction du code entré par l'utilisateur

        - **paramètres**, **types**, **retour** et **types de retour**::
          
        :param code: code produit 
        :type code: str of int
        :return: None
        :rtype: NoneType
        """
        self.produit_choisi = self.base_produits.retourneProduit(code)
    
    def refus_entree(self, nouv_loc):
        """
        Teste si l'arrivée de l'utilisateur au menu Contributeur ou Administrateur
        doit être refusée.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param nouv_loc: localisation désirée
        :type nouv_loc: list of 2 str
        :return: valeur du test (True si refus)
        :rtype: bool
        """
        return ((nouv_loc[1] == '6') &\
        (type(self.navigateur) != Administrateur)) | \
        ((nouv_loc[1] == '2') &\
        (type(self.navigateur) not in (Contributeur,Administrateur)))
    
    def autorisationNavigateur(self, nouv_loc):
        """
        Méthode qui contrôle l'accès au menu de contribution ou d'administration,
        aussi bien en amont de l'arrivée sur le menu (évite qu'un contributeur 
        ne soit obligé de se connecter à chaque fois qu'il revient à l'accueil)
        qu'après demande de connexion.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param nouv_loc: localisation désirée
        :type nouv_loc: list of 2 str
        :return: valeur du test (True si refus)
        :rtype: bool
        """
        #si refus avant connexion
        if((nouv_loc[0] == 'MENU') & self.refus_entree(nouv_loc)): 
            self.setNavigateur(self.connexion())
        #si refus après tentative de connexion
        return not (self.refus_entree(nouv_loc)) 
    
    def setLocalisation(self, nouv_loc):
        """
        Modifie la localisation de l'utilisateur après vérification de
        l'autorisation.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param nouv_loc: localisation désirée
        :type nouv_loc: list of 2 str
        :return: retourne si la modification a pu être opérée
        :rtype: bool
        """
        if self.autorisationNavigateur(nouv_loc):
            self.localisation = nouv_loc
            return True
        else:
            return False
    
    def creationCompteUtilisateur(self, codes):
        """
        Création d'un compte utilisateur en fonction des codes entrés 
        par l'utilisateur.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param codes: contient le pseudo et le mot de passe séparés par un espace.
        :type codes: str
        :return: Vrai si l'ajout a pu être fait
        :rtype: bool
        
        .. seealso:: :class:`BaseUtilisateurs`
        """
        try:
            contrib = Contributeur(codes.split(' ')[0],codes.split(' ')[1])
            self.base_utilisateurs.ajouterContributeur(contrib)
        except:
            print("\n"+"-"*60+"\nLe pseudo et le mot de passe doivent"+\
            " être séparés par un espace."+"\n"+"-"*60+"\n")    
    
    def boxplotContrib(self):
        """
        Affiche le boxplot des contributions à la base de données des produits.
        Les outliers - les points extrêmes - ne sont pas affichés sur le graphique.
        
        .. seealso:: :class:`BaseProduits`        
        """
        contribs = np.array(self.base_produits.compterAllContrib())
        green_diamond = dict(markerfacecolor='g', marker='D')
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 10))
        ax.set_title('Distribution des contributions à la base des produits')
        bplot = ax.boxplot(contribs, showfliers=False, showmeans = True,\
        patch_artist = True, vert = False, meanprops = green_diamond)
        colors = ['lightgreen']
        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)
        plt.show()
        # plt.savefig('boxplot.png')
        # print("Le graphique est exporté en png")
        
    def statProdHorsFrance(self):
        """
        Appel de la méthode `BaseProduits.afficherStatCompterPays()`.
        Affiche les résultats de la méthode à l'écran.
        
        .. seealso:: :class:`BaseProduits`
        """
        print(self.base_produits.afficherStatCompterPays())
        
    def afficherStatIngredient(self, ingredient):
        """
        Appel de la méthode `BaseProduits.afficherStatIngredient()
        
        - **paramètres**, **types**::
        
        :param ingredient: ingrédient recherché par l'utilisateur
        :type ingredient: str
        
        .. seealso:: :class:`BaseProduits`
        """
        print(self.base_produits.afficherStatIngredient(ingredient))
    
    def accueil(self):
        """
        Affiche le menu d'accueil de l'application.
        
        .. seealso:: :class:`Menu`
        """
        os.system('cls')
        self.localisation = ['MENU','0']
        return(self.base_menus['0'].afficher())
        
    def quitter(self):
        """
        Permet de quitter l'application.
        """
        print("-"*37 + "\nMerci de votre visite. A bientôt.\n"\
        +"-"*37)
        sys.exit(0)
        
    def afficherEcran(self, choix):
        """
        Cette méthode gère l'affichage et donc l'enchaînement des écrans
        en récupérant les informations contenues dans les attributs des différentes
        classes d'écrans.
        La méthode conclut en appelant la méthode `afficher()` de la classe qui
        convient.
        
        -Si choix = A : retour à l'accueil
        -Si choix = R : retour au menu propre à chaque type d'utilisateur
        -Si choix = Q : retourne 'Q' (qui induit ailleurs la fermeture)
        -Sinon : retourne l'écran qui convient.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param choix: Texte entré par l'utilisateur
        :type choix: str
        :return: retourne l'écran à afficher sous forme de str
        :rtype: str
        
        .. seealso:: :class:`Menu`, :class:`Prompt`, :class:`Modif`
        
        """
        if choix.upper() == 'A': #accueil
            self.setLocalisation(['MENU','0'])
            return self.accueil()
        elif choix.upper() == 'R': #retour
            if type(self.navigateur) == Contributeur:
                self.setLocalisation(['MENU','2'])
                return self.base_menus['2'].afficher()
            elif type(self.navigateur) == Administrateur:
                self.setLocalisation(['MENU','6'])
                return self.base_menus['6'].afficher()
            else:
                self.setLocalisation(['MENU','0'])
                return self.base_menus['0'].afficher()
        else:
            type_origine = self.getLocalisation()[0]
            num_origine = self.getLocalisation()[1]
            if type_origine == 'MENU': #on vient d'un menu
                origine = self.base_menus[num_origine]
                try:
                    type_destination = origine.getTypeSuivants()[choix]
                    num_destination = origine.getSuivants()[choix]
                except:
                    return(input(self.message_choix_incorrect))            
            elif type_origine == 'PROMPT': #on vient d'un text prompt
                origine = self.base_prompts[num_origine]
                try:
                    type_destination = origine.getTypeSuivant()
                    num_destination = origine.getSuivant()
                except:
                    return(input(self.message_choix_incorrect))
                if origine.getAvantDestination() != 'X':
                    exec(origine.getAvantDestination())
            elif type_origine == 'MODIF': #on vient d'une modif
                origine = self.base_modifs[num_origine]
                try:
                    type_destination = origine.getTypeSuivant()
                    num_destination = origine.getSuivant()
                except:
                    return(input(self.message_choix_incorrect))
                champ = origine.getChamp()
                nouv_val = choix
                code_modif = self.produit_choisi.getCode()
                self.base_produits.modifierProduit(code_modif, champ, nouv_val)
                if champ == 'code':
                    self.setProduitChoisi(nouv_val)
                else:
                    self.setProduitChoisi(code_modif)
            else:
                return 'Q'
            
            if(type_destination == 'menu'): #on va vers un menu
                if self.setLocalisation(['MENU',num_destination]) :
                    return self.base_menus[num_destination].afficher()
                else:
                    return origine.afficher()
            elif(type_destination == 'prompt'): #on va vers un text prompt
                self.setLocalisation(['PROMPT',num_destination])
                destination = self.base_prompts[num_destination]
                if destination.getAvantPrompt() != 'X':
                    exec(destination.getAvantPrompt())
                return destination.afficher()
            elif(type_destination == 'modif'): #on va vers un text prompt
                self.setLocalisation(['MODIF',num_destination])
                val_defaut = eval(self.base_modifs[num_destination].getDefaut())
                return self.base_modifs[num_destination].afficher(val_defaut)
            elif(type_destination == 'fonction'): #on exécute une fonction
                try:
                    exec(num_destination)
                except:
                    return(input(self.message_choix_incorrect))
                return origine.afficher()
            else:
                return 'Q'           
        
        
    def chargementMenus(self, chemin):
        """
        Charge en mémoire une base de menus et modifie l'attribut base_menus
        de l'instance de la classe Application.
        
        - **paramètres**, **types**, **retour**::
        
        :param chemin: répertoire + nom du fichier
        :type chemin: str
        :return: None
        """
        menus = dict()
        numeros = dict()
        intitules = dict()
        titres = dict()
        suivants = dict()
        type_suivants = dict()
        with open(chem_data+chemin, 'r', encoding='utf-8') as infile:
            next(infile)
            for line in infile:
                champs = line.strip().split('\t')
                if champs[1] == '1':
                    numeros[champs[0]] = list()
                    intitules[champs[0]] = list()
                    titres[champs[0]] = list()
                    type_suivants[champs[0]] = list()
                    suivants[champs[0]] = list()
                numeros[champs[0]].append(champs[1])
                intitules[champs[0]].append(champs[2])
                titres[champs[0]].append(champs[3])                
                type_suivants[champs[0]].append(champs[4])
                suivants[champs[0]].append(champs[5])
                
            for num_menu in numeros.keys():
                menus[num_menu] = Menu(numeros[num_menu], intitules[num_menu], \
                titres[num_menu][0], type_suivants[num_menu], suivants[num_menu])
        
        self.base_menus = menus
        
    def chargementPrompts(self, chemin):
        """
        Charge en mémoire une base de prompts et modifie l'attribut base_prompts
        de l'instance de la classe Application.
        
        - **paramètres**, **types** et **retour**::
        
        :param chemin: répertoire + nom du fichier
        :type chemin: str
        :return: None
        """
        prompts = dict()
        with open(chem_data+chemin, 'r', encoding='utf-8') as infile:
            next(infile)
            for line in infile:
                numero, intitule, avant_prompt, avant_destination,\
                type_suivant, suivant = line.strip().split('\t')
                prompts[numero] = Prompt(intitule, avant_prompt,\
                avant_destination, type_suivant, suivant)
                
        self.base_prompts = prompts
        
    def chargementModifs(self, chemin):
        """
        Charge en mémoire une base de modifs et modifie l'attribut base_modifs
        de l'instance de la classe Application.
        
        - **paramètres**, **types** et **retour**::
        
        :param chemin: répertoire + nom du fichier
        :type chemin: str        
        :return: None
        """
        modifs = dict()
        with open(chem_data+chemin, 'r', encoding='utf-8') as infile:
            next(infile)
            for line in infile:
                numero, champ, intitule, defaut, avant_destination,\
                type_suivant, suivant = line.strip().split('\t')
                modifs[numero] = Modif(champ, intitule, defaut,\
                avant_destination, type_suivant, suivant)
                
        self.base_modifs = modifs
        
    def chargementUtilisateurs(self, chemin):
        """
        Charge en mémoire une base d'utilisateurs et modifie l'attribut 
        base_utilisateurs de l'instance de la classe Application.
        
        - **paramètres**, **types** et **retour**::
        
        :param chemin: répertoire + nom du fichier
        :type chemin: str        
        :return: None
        """
        base_utilisateurs = BaseUtilisateurs()
        with open(chem_data+chemin, 'r', encoding='utf-8') as infile:
            next(infile)
            #infile est un iterable : next permet de sauter la première ligne
            for line in infile:
                pseudo, mdp, statut, etat = line.strip().split('\t')
                if statut == 'C':
                    contrib = Contributeur(pseudo,mdp,statut,etat)
                    base_utilisateurs.ajouterContributeur(contrib)
                elif statut == 'A':
                    admin = Administrateur(pseudo,mdp,statut,etat)
                    base_utilisateurs.ajouterContributeur(admin)
                else:
                    print("statut inconnu")
        self.base_utilisateurs = base_utilisateurs
    
    def chargementProduits(self, chemin):
        """
        Charge en mémoire une base de produits et modifie l'attribut 
        base_produits de l'instance de la classe Application.
        
        - **paramètres**, **types** et **retour**::
        
        :param chemin: répertoire + nom du fichier
        :type chemin: str        
        :return: None
        """
        base_produits = BaseProduits()
        with open(chem_data+chemin, 'r', encoding='utf-8') as infile:
            headers = infile.readline().strip().split('\t')
            for line in infile:
                code, creator, created_t, last_modified_t, product_name,\
                quantity, manufacturing_places, stores, countries,\
                ingredients_text = line.split('\t')
                produit = Produit(code, creator, created_t, last_modified_t,\
                product_name, quantity, manufacturing_places, stores, countries,\
                ingredients_text.strip().replace('_',''))
                base_produits.ajouterProduit(produit)
                
        self.base_produits = base_produits