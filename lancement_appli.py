
if __name__ == '__main__':
    import os
    import sys
    import collections
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    
    #v36_perso
    from pyautogui import typewrite

    chem_pgms = os.getcwd()+'\\pgms'
    chem_data = os.getcwd()+'\\data'
    sys.path.append(chem_pgms)
    sys.path.append(chem_data)

    from class_appli import *
    from class_ecran import *
    from class_utilisateurs import *
    from class_produit import *
    
    # os.system("pause")

    #instance de l'application
    appli = Application()
    #instances des menus
    appli.chargementMenus('\\menus.csv')
    #instances des prompts
    appli.chargementPrompts('\\prompts.csv')
    #instances des modifs
    appli.chargementModifs('\\modifs.csv')
    #les utilisateurs
    appli.chargementUtilisateurs('\\base_utilisateurs.csv')
    #les produits
    appli.chargementProduits('\\open_food_facts.csv')
    
    choix = appli.accueil()

    while( choix.upper() != 'Q'):
        choix = appli.afficherEcran(choix)
        
    appli.quitter()
