import os
import sys
from distutils.core import setup
from cx_Freeze import setup, Executable
import matplotlib


# Dependencies are automatically detected, but it might need fine tuning.
# build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = "Console"


# C:\\Users\\mrteste\\AppData\\Local\\Programs\\Python\\Python36-32\\DLLs\\
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ["TCL_LIBRARY"] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ["TK_LIBRARY"] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')


build_exe_options = {"includes":["matplotlib.backends.backend_agg",'numpy.core._methods', 'numpy.lib.format'],
                     # "packages":["tkinter", "tkinter.filedialog"],
                     # "include_files":[(matplotlib.get_data_path(), "mpl-data"),
                     # os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
                     # os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll')]
                     }

setup(  name = "poo_product_ensai",
        version = "0.1",
        description = "standalone",
        options = {'build_exe': build_exe_options},
        executables = [Executable("lancement_appli.py", base=base)])