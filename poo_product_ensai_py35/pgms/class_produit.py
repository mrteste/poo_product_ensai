# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 13:51:03 2019

@author: id1313
"""

import time

class Produit:
    """
    
    Une instance de la classe Produit dispose des attributs correspondant au
    champ présent dans la base de données `open_food_facts.csv`.
      
    La classe dispose des méthodes get et set associés à chacun des attributs,
    et surcharge la méthode `__str__` pour gérer l'affichage d'un produit.
    
    .. seealso:: :class:BaseProduits
    
    """
    def __init__(self, code, creator = '', created_t = time.time(),\
                last_modified_t = time.time(), product_name = '', quantity = '',\
                manufacturing_places = '', stores = '', countries = '', \
                ingredients_text=''): #variables):
        """
        Attributs
        ---------
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param code: code-barre - composé seulement de chiffres
        :type code: str
        :param creator: pseudo du créateur de la fiche produit
        :type creator: str
        :param created_t: date de création - nombre de secondes depuis une
        date de référence
        :type created_t: float
        :param last_modified_t: date de la dernière modification - en secondes
        :type last_modified_t: float
        :param product_name: nom du produit
        :type product_name: str
        :param quantity: quantité
        :type quantity: str
        :param manufacturing_places: lieux de fabrication
        :type manufacturing_places: str
        :param stores: magasins de vente
        :type stores: str
        :param countries: pays de vente
        :type countries: str
        :param ingredients_text: ingrédients composant le produit
        :type ingredients_text: str
            
        """
        
        try:
            int(code)
            self.code = code
            self.creator = creator
            self.created_t = created_t
            self.last_modified_t = last_modified_t
            self.product_name = product_name
            self.quantity = quantity
            self.manufacturing_places = manufacturing_places
            self.stores = stores
            self.countries = countries
            self.ingredients_text = ingredients_text
        except:
            print("un code ne doit être constitué que de chiffres")
            return(None)
    
    
    def setCode (self, nv_code):
        try:
            int(nv_code)
            self.code = nv_code
        except:
            print('Code non valable: un code ne doit être constitué que de chiffres')
        
    def getCode (self):
        return self.code
    
    def getCreator (self):
         return self.creator
         
    def setCreator (self, nv_creator):
        self.creator = nv_creator
         
    def getCreated (self):
         return self.created_t
         
    def getLastModified (self):
        return self.last_modified_t
    
    def setLastModified (self,nv_date):
        self.date=nv_date

         
    def getQuantity (self):
        return self.quantity
    
    def setQuantity (self, nv_quant):
        self.quantity = nv_quant
    
    def setProductName(self, nv_name):
        self.product_name = nv_name

         
    def getProductName (self):
        return self.product_name
        
    def setManufacturingPlaces (self, nv_manuf = ''):
        self.manufacturing_places = nv_manuf
    
    def getManufacturingPlaces (self):
        return self.manufacturing_places
         
    def setStores(self, nv_store = ''):
         self.stores = nv_store
         
    def getStores(self):
         return self.stores        
    
    def setCountries(self, nv_country = ''):
        self.countries = nv_country 
        return self.countries
         
    def getCountries(self):
         return self.countries
    
    def setIngredients (self, nv_ing = []):
         return self.ingredients_text
         
    def getIngredients (self):
       return self.ingredients_text
       
    def setTimeString(self, t):
        """
        Méthode qui permet de passer d'une date exprimée en seconde à une date
        exprimée dans un format lisible par l'utilisateur.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param t: temps en secondes (généré par time.time() par exemple)
        :type t: float
        :return: temps affiché au format `%d %b %y %H:%M:%S`
        :rtype: str
        
        .. note:: les deux attributs de date sont conservés en *float* car cela
            permet des comparaisons et des tris aisés.
        """        
        return time.strftime("%d %b %y %H:%M:%S", time.gmtime(int(t)))

    def __str__ (self):
        """
        Surcharge de la méthode `__str__`
        """
        creation_str = self.setTimeString(self.getCreated())
        modification_str = self.setTimeString(self.getLastModified())
        return '\
        - Code Produit: %s \n\
        - Nom du Produit: %s \n\
        - Créateur: %s \n\
        - Date de creation: %s \n\
        - Dernière date de modification: %s \n\
        - Quantité: %s \n\
        - Pays Fabrication: %s \n\
        - Magasin de vente: %s (Pays: %s) \n\
        - Ingrédients: %s\
        ' % (self.getCode(), self.getProductName(), self.getCreator(), \
        creation_str, modification_str, \
        self.getQuantity(), self.getManufacturingPlaces(), \
        self.getStores(), self.getCountries(), self.getIngredients())
    
        
###############################################################################

class BaseProduits:
    """
    
    La classe BaseProduits permet de gérer l'ensemble des produits issus
    de la base `open_food_facts.csv`. Elle compte des méthodes de gestion 
    (`ajouterProduit`, `supprimerProduit` et `modifierProduit`), des méthodes 
    d'extraction d'informations (`retourneProduit`, `chercherNDerniersProduits`,
    `compterContrib`, `compterAllContrib`, `trouverIngredient`) et des méthodes
    d'affichage (`afficherUnProduit`,`afficherIngredientsUnProduit`,
    `afficherNDerniersProduits`, `afficherStatCompterPays`,
    `afficherStatIngredient`).
    
    La classe dispose des méthodes get et set associés à chacun des attributs,
    et surcharge la méthode `__str__` pour gérer l'affichage d'un produit.
    
    .. seealso:: :class:class_apppli.Application qui gère le chargement des 
        données produits sous la forme d'une instance de :class:BaseProduits
    
    .. note:: Le seul attribut d'instance est un dictionnaire composé d'instances
        de la classe Produit (values) dont la clé est le code du produit (keys).
    
    .. note:: La classe dispose d'un attribut de classe.
        `france` est une liste (`list`) composée des termes qui, dans la table des 
        données fournies, permettent d'identifier la France comme pays de production.
    
    """
    france = ['FRANCE','FRANCIA','FRANKRIJK','MAYOTTE','GUADELOUPE','POLYN']

    def __init__(self):
        """
        L'initialisation d'une instance de :class:BaseProduits se fait à vide.
        
        .. seealso:: méthode `ajouterProduit` est ainsi la seule méthode pour
        enrichir l'instance.
        
        """
        self.base = dict() 


    def existeCode(self, nv_code):
        """
        Teste si un produit est effectivement dans la base, à partir de la
        connaissance de son code-barre.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param nv_code: le code à tester
        :type nv_code: str
        :return: True si le code fourni est bien le code d'un produit de la base
        :rtype: bool
        
        """
        return nv_code in self.base.keys()
    
        
    def ajouterProduit (self, produit):
        """
        Ajoute un nouveau produit à la base, après avoir testé qu'il n'était pas
        déjà dans la base.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param produit: code du produit a ajouter
        :type int: code
        :param pseudo: pseudo de l'utilisateur
        :type pseudo: str
        :return: True si l'ajout a pu être fait, False sinon
        :rtype: bool
        
        """
        if self.existeCode(produit.getCode()) == False:
            self.base[produit.getCode()] = produit
            return True
        else:
            print("Code déjà existant")
            return False
            
        
    def supprimerProduit (self, code_supp):
        """
        Supprime un produit de la base, après avoir vérifié qu'il était dedans.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param code_supp: code du produit qu'on veut supprimer
        :type code_supp: str
        :return: True si la suppression a été possible, False sinon
        :rtype: bool
        
        """
        if self.existeCode(code_supp) == True:
            del self.base[code_supp]
            print("Le produit avec le code "+str(code_supp)+" a bien été supprimé")
            return True
        else:
            print("Code inexistant")
            return False
    
            
    def modifierProduit (self, code_modif, champ, valeur):
        """
        Modifie un produit de la base, en précisant le champ à modifier.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param code_modif: le code du produit qu'on veut modifier
        :type code_modif: str - composé uniquement de chiffres
        :param champ: le champ que l'on veut modifier 
        :type champ: str
        :param valeur: la donnée qui va remplacer l'actuelle valeur du champ
        :type valeur: str
        
        .. seealso:: :class:Modif
        
        .. note:: Les instances de Produit d'une instance de BaseProduit étant
            rassemblées dans un dictionnaire (l'attribut base), le code servant de clé,
            la modification du code n'est pas possible autrement qu'en supprimant
            et ajoutant le produit. Pour les autres champs, la modification de
            l'attribut de produit correspondant suffit.           
        
        """
        if self.existeCode(code_modif):
            setattr(self.base[code_modif], 'last_modified_t', time.time())
            if champ == 'code':
                self.base[code_modif].setCode(valeur)
                produit = self.base[code_modif]
                self.supprimerProduit(code_modif)
                self.ajouterProduit(produit)
            else:
                setattr(self.base[code_modif], champ, valeur)
            return True
        return False
    
    def afficherIngredientsUnProduit(self, code_affich):
        """
        Affiche les ingrédients d'un produit à partir du code du produit.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param code_affich: code du produit dont on veut afficher les ingrédients
        :type code_affich: str
        :return: None
        :rtype: NoneType
        
        """
        if self.existeCode(code_affich):
            produit = self.base[code_affich]
            print('Le produit %s (code : %s) est composé des ingrédients suivants :\
        \n %s' %(produit.getProductName(), code_affich, produit.getIngredients()))
        else:
            print('Code inexistant')

    
    def retourneProduit(self, code):
        """
        Retourne le produit dont le code-barre correspond au code renseigné.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param: code: le code du produit qu'on veut rechercher
        :type code: str
        :return: None
        :rtype: NoneType
        """
        return self.base[code] if self.existeCode(code) else False
        
    def chercherNDerniersProduits(self, n = 10):
        """
        Retourne les n produits les plus récents
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param n: nombre de produits souhaités
        :type: int
        :return: codes des n produits les plus récents
        :rtype: list
        """
        creations = list()
        for code, produit in self.base.items():
            creations.append((produit.created_t, produit))
        return(sorted(creations, key=lambda x: x[0], reverse = True)[0:n])

    def afficherNDerniersProduits(self, n = 10):
        """
        Affiche les n produits les plus récents
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param n: nombre de produits souhaités
        :type: int
        :return: None
        :rtype: NoneType
        
        .. seealso:: :class:Produit et sa méthode `__str__`
        
        """
        dernieres_creations = self.chercherNDerniersProduits(n)
        print("---------------------")
        for produit in dernieres_creations:
            print(produit[1])
            print("---------------------")
            

    def compterPays(self):
        """
        Dénombre les produits fabriqués en France et ceux fabriqués à l'étranger.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :return: nombre et pourcentage de produits fabriqués en France, 
            à l'étranger, ou dont l'information est manquante.
        :rtype: list
        
        .. note:: Un produit au moins partiellement fabriqué en France est considéré
            comme fabriqué en France. Ainsi, les produits fabriqués à l'étranger
            sont les produits qui ne sont pas du tout fabriqués en France.
            
        .. note:: Les données manquantes étant possibles, la méthode dénombre 
            également les produits pour lesquels l'information est manuqante.
        """
        fr=0
        NA = 0
        etr = 0
        for prod in self.base.values():
            liste = prod.getManufacturingPlaces().upper().split(',')
            if [''] == liste:
                NA+=1
            else:
                a = False
                for l in liste:
                    for f in self.france:
                        if f == l:
                            a = True
                            fr += 1
                            break
                if a == False:
                    etr += 1
        return [['France','Etranger','Inconnu', 'Total'],
                [fr,etr,NA,fr+etr+NA],
                [round(fr/(fr+etr+NA)*100,2), round(etr/(fr+etr+NA)*100,2), 
                round(NA/(fr+etr+NA)*100,2), 100]]

    
    def afficherStatCompterPays(self):
        """
        Renvoie le tableau des résultats de la méthode compterPays
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :return: tableau des effectifs et pourcentage des produits par lieux de
            fabrication.
        :rtype: str       
        
        """
        stats = self.compterPays()
        horiz1 = "-" * 60
        horiz2 = "-" * 68
        titre = "| " + "Nombre de produits selon leur lieu de fabrication"\
        .center(56) + " |"
        entete = " ".center(12) + " | " + stats[0][0].center(10) + " | "+\
        stats[0][1].center(10) + " | " +\
        stats[0][2].center(10) + " | " + stats[0][3].center(10)
        entete_complet = "| " + entete.center(61) + " |"
        ligne1 = "Nb produits".center(12) + " | " + str(stats[1][0]).center(10)\
        + " | " + str(stats[1][1]).center(10) + " | " +\
        str(stats[1][2]).center(10) + " | " + str(stats[1][3]).center(10)
        ligne1_complet = "| " + ligne1.center(61) + " |"
        ligne2 = "Part en %".center(12) + " | " + str(stats[2][0]).center(10)\
        + " | " + str(stats[2][1]).center(10) + " | " +\
        str(stats[2][2]).center(10) + " | " + str(stats[2][3]).center(10)
        ligne2_complet = "| " + ligne2.center(61) + " |"
        contenu = [horiz1,titre,horiz1,'',horiz2,entete_complet,horiz2,\
        ligne1_complet,horiz2,ligne2_complet,horiz2]
        return('\n'.join(contenu))
        

    def afficherStatIngredient(self, chaine):
        """
        Renvoie le tableau des résultats de la méthode trouverIngredient
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param chaine: l'ingrédient recherché
        :type chaine: str
        :return: Nombre de produits concernés et liste des codes et noms
        :rtype: str 
        
        """
        liste = self.trouverIngredient(chaine)
        codenom = []
        for i in range(len(liste)):
            codenom.append(self.retourneProduit(liste[i]).getProductName())
        horiz = '-'*71
        vide1 = "| " + ' ' * 67 + ' |'
        titre = "| " + ("Produits ayant '%s' dans leur liste d'ingrédients"\
                        %(chaine)).center(67)+" |"
        T = 'TOTAL:' + str(len(liste))
        T = '| ' + T.ljust(10) + ' '*57 +' |'
        nom = '| ' + 'Code du produit'.center(15) +'| '+ 'Nom du produit'.\
        center(50) + ' |'
        total =[horiz , vide1 , titre , vide1 , T , horiz, nom, horiz]
        for i in range(len(liste)):
            codei = str(liste[i]).ljust(15)
            nomi = codenom[i].ljust(50)
            lignei = '| ' +  codei.center(15) +'| '+nomi.center(50) + ' |'            
            total.append(lignei)
            total.append(horiz)
        return '\n'.join(total)
               
    
    def compterContrib(self, pseudo):
        """
        Dénombre les contributions d'un pseudo donné à la construction de la base,
        c'est-à-dire compte le nombre de fois qu'un pseudo est présent comme
        créateur de la fiche-produit.
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param pseudo: le pseudo dont on veut connaître les contributions 
        :type pseudo: str
        :return: nombre de contributions
        :rtype: int
        """
        c=0
        for prod in self.base.values():
            if prod.getCreator() == pseudo:
                c +=1
        return c
        
    def compterAllContrib(self):
        """
        Renvoie la liste de contributions
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :return: nombre de contributions de l'ensemble des contributeurs à la base.
        :rtype: list of int
        """
        pseudos_uniques = set([self.base[code].getCreator() for code in self.base.keys()])
        return [self.compterContrib(pseudo) for pseudo in pseudos_uniques]

    def normalisation(self, chaine):
        """
        Renvoie une chaîne de caractères normalisée mise en majuscule en 
        remplaçant '_' par ' ' et en supprimant les espaces en début de chaîne
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param chaine: la chaîne de caractères à normaliser
        :type chaine: str
        :return: chaîne normalisée
        :rtype: str
        
        """
        return chaine.replace('_',' ').strip(' ')

    
    def trouverIngredient(self, chaine):
        """
        Renvoie le nom et le code des produits ayant dans sa liste d'ingrédients 
        la chaine de caractère précisée
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param chaine: la chaîne de caractères à chercher dans les ingrédients
        :type chaine: str
        :return: codes des produits contenant la chaîne recherchée
        :rtype: list
        """
        chaine = self.normalisation(chaine)
        liste = []
        for cle in self.base.keys():
            if chaine in [self.normalisation(i) for i in self.retourneProduit(cle)\
                          .getIngredients().split(', ')]:
                liste.append(cle)
        return liste
     

if __name__ == "__main__":
    
    base_produit = BaseProduits()
    base_produit.ajouterProduit(Produit(256000))
    base_produit.modifierProduit(256000, 'creator', 'mrteste')
    base_produit.modifierProduit(256000, 'ingredients_text', 'chou, fleur')
    base_produit.modifierProduit(256000, 'creator', 'mrteste')
    base_produit.ajouterProduit(Produit(123456))
    base_produit.modifierProduit(123456, 'creator', 'AZERTY')
    base_produit.modifierProduit(123456, 'ingredients_text', 'D')
    base_produit.modifierProduit(123456, 'manufacturing_places', 'Italie, Francia, France')
    base_produit.modifierProduit(256000, 'manufacturing_places', 'France')
    base_produit.ajouterProduit(Produit(123))
    base_produit.modifierProduit(123, 'manufacturing_places', 'France, Francia')
    base_produit.ajouterProduit(Produit(456))
    base_produit.modifierProduit(456, 'manufacturing_places', 'France')
    base_produit.modifierProduit(456, 'creator', 'mrteste')
    base_produit.ajouterProduit(Produit(6789))    
    base_produit.modifierProduit(6789, 'manufacturing_places', 'UK, France')
    base_produit.modifierProduit(6789, 'ingredients_text', '_fleur______, D')
    base_produit.modifierProduit(6789, 'product_name', 'lila')

    base_produit.ajouterProduit(Produit(10))    
    base_produit.modifierProduit(10, 'manufacturing_places', 'UK, Italie')
    base_produit.modifierProduit(10, 'ingredients_text', 'fleur')
    base_produit.modifierProduit(256000, 'product_name', 'tulipe')
    base_produit.modifierProduit(10, 'product_name', 'pensées')

    base_produit.ajouterProduit(Produit(2560100))
    base_produit.compterAllContrib()
    
    print(base_produit.afficherStatIngredient('fleur'))
    

    print(base_produit.base[123456])
    base_produit.afficherNDerniersProduits()
    #base_produit.supprimerProduit(123456)
    base_produit.compterContrib('mrteste')
    base_produit.compterPays()
    base_produit.retourneProduit(123456)    
    base_produit.retourneProduit(256000).getCreator()
    base_produit.normalisation(base_produit.retourneProduit(6789).getIngredients()[0])

    base_produit.trouverIngredient('fleur')
    
    print(base_produit.afficherStatIngredient('fleur'))
    
    Produit(256000).getLastModified()

    # base_produit.afficherUnProduit(256000)    
    print(base_produit.base[256000])
    base_produit.base[256000].getIngredients()
    print(base_produit.afficherIngredientsUnProduit(256000))
    
