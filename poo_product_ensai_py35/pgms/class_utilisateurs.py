# -*- coding: utf-8 -*-
"""
Created on Tue May 14 10:39:34 2019

@author: id1232
"""

class Personne():
    """
    Classe des personnes
    """
    def __init__(self,localisation="0"):
        """
        Initialisation de la classe
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param localisation: Localisation de la personne dans le programme
        :type localisation: str
        """
        self.localisation=localisation
        
    def getLocalisation(self):
        return self.localisation
        
    def setLocalisation(self,nouvelleLocalisation):
        """
        Modifie la localisation de la personne
        """
        self.localisation=nouvelleLocalisation
        
        
class Contributeur(Personne):
    """
    Classe des contributeurs
    """
    def __init__(self,pseudo,mdp,statut='C',etat='N'):
        """
        Initilisation de la classe
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param pseudo: Pseudonyme du contributeur
        :type pseudo: str
        :param mdp: Mot de passe du contributeur
        :type mdp: str
        :param statut: Statut du contributeur (A: administrateur ou 
        C: contributeur)
        :type statut: str
        :param etat: État de la validation du compte du contributeur 
        (N: non validé ou V: validé)
        """
        Personne.__init__(self)
        self.pseudo=pseudo
        self.mdp=mdp
        self.statut=statut
        self.etat=etat
        
    def getPseudo(self):
        return self.pseudo
        
    def getMdp(self):
        return self.mdp
        
    def getStatut(self):
        return self.statut
        
    def getEtat(self):
        return self.etat
        
    def setEtat(self,nouveauEtat):
        self.etat=nouveauEtat
      
    def __str__(self):
        return ', '.join([self.pseudo,self.mdp,self.statut,self.etat])
        

class Administrateur(Contributeur):
    """
    Classe des aministrateurs
    """
    def __init__(self,pseudo,mdp,statut='A',etat='N'):
        Contributeur.__init__(self,pseudo,mdp,statut,etat)
        
        
class BaseUtilisateurs():
    """
    Classe de la base des utilisateurs qui comprend tous les comptes 
    contributeurs et administrateurs (validés ou non)
    """
    def __init__(self):
        """
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param dicoUtilisateurs: dictionnaire des comptes avec le pseudo en clé
        clé = pseudo; valeur = contributeur ou administrateur
        :type: dicoUtilisateurs: dict
        """
        self.dicoUtilisateurs=dict()
        
    def ajouterContributeur(self,contrib):
        """
        Méthode pour ajouter un compte à la base
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param contrib: objet du contributeur à ajouter à la base
        :type contrib: objet
        """
        char1=" a bien été ajouté à la base utilisateur"
        char2="Erreur, l'utilisateur n'a pas pu être ajouté à "
        char3="la base de données.\n"+"-"*45+"\n"
        try:
            if contrib.getPseudo() in self.dicoUtilisateurs.keys():
                print("\n"+"-"*45+"\nCe pseudo existe déjà."+
                "\nVeuillez en choisir un autre.\n"+"-"*45+"\n")
                return False
            else:
                self.dicoUtilisateurs[contrib.getPseudo()]=contrib
                print("L'utilisateur "+contrib.getPseudo()+char1)
        except:
            print(char2+char3)
            return False
                
    def supprimerContributeur(self,pseudo,pseudo_navigateur):
        """
        Méthode pour retirer un compte de la base des utilisateurs
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param pseudo: pseudo du compte à retirer de la base des utilisateurs
        :type pseudo: str
        :param pseudo_navigateur: pseudo de celui qui demande la suppression
        :type pseudo_navigateur: str
        """
        char1=" a bien été enlevé de la base utilisateur"
        char2=" n'appartient pas à la base utilisateur et "
        char3="ne peut donc pas être supprimé"
        if pseudo == pseudo_navigateur:
            print("Vous ne pouvez pas supprimer votre propre compte")
        else:
            try:
                del self.dicoUtilisateurs[pseudo]
                print("L'utilisateur "+pseudo+char1)
            except:
                print("L'utilisateur "+pseudo+char2+char3)
        
    def modifierEtats(self,pseudo,etat):
        """
        Méthode pour modifier l'état (valide ou non) d'un utilisateur
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param pseudo: pseudo de l'utilisateur dont on veut modifier l'état
        :type pseudo: str
        :param etat: etat final de l'utilisateur
        :type etat: str
        """
        char1=" ne fait pas partie de la base des utilisateurs"
        if etat=="N":
            texte1=pseudo+" a déjà son compte non validé"
            texte2="Le compte de "+pseudo+" est non validé."
        else:
            texte1=pseudo+" a déjà son compte validé"
            texte2="Le compte de "+pseudo+" est validé." 
        if pseudo in [pseudos for pseudos in self.dicoUtilisateurs.keys()]:
            if self.dicoUtilisateurs[pseudo].getEtat()==etat:
                print(texte1)
            else:
                try:
                    self.dicoUtilisateurs[pseudo].setEtat(etat)
                    print(texte2)
                except:
                    print("Erreur, l'état n'a pas pu être changé.")
                    return False
        else:
            print("L'utilisateur "+pseudo+char1)
            return False
          
    def validerCompte(self,listeComptesAValider,admin):
        """
        Méthode permettant de valider des comptes à partir d'une liste entrée 
        par l'utilisateur
        Cette méthode va utiliser la méthode modifierEtats
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param listeComptesAValider: liste de comptes à valider
        :type listeComptesAValider: list
        :param admin: pseudo d'un administrateur
        :type admin: str
        """
        char1="Attention. Seuls les administrateurs ont l'autorisation "
        char2="de valider des comptes."
        if type(admin)==Administrateur:
            for contrib in listeComptesAValider:
                self.modifierEtats(contrib,"V")
        else:
            print(char1+char2)
            return False
            
    def recupererEtatsNonValides(self):
        """
        Méthode pour récupérer l'ensemble des comptes non encore validés
        Si le programme ne trouve pas de compte non validé, il renvoie le 
        message "Tous les comptes sont déjà validés"
        """
        ENV=[]
        for cle in self.dicoUtilisateurs.keys():
            if self.dicoUtilisateurs[cle].getEtat()=="N":
                ENV.append(cle)
        if ENV==[]:
            print("Tous les comptes sont déjà validés.")
        else:
            return ENV
            
    def verificationSiDansBase(self,pseudo,mdp):
        """
        Méthode pour vérifier si une combinaison pseudo et mot de passe est 
        dans la base utilisateurs
        
        - **paramètres**, **types**, **retour** et **types de retour**::
        
        :param pseudo: pseudo à vérifier
        :type pseudo: str
        :param mdp: mot de passe à vérifier
        :type mdp: str
        """
        if pseudo in self.dicoUtilisateurs.keys():
            return mdp == self.dicoUtilisateurs[pseudo].getMdp()
        else:
            return False
            
    def __str__(self):
        horiz = "-" * 53
        titre = "| " + "Comptes utilisateurs".center(49) + " |"
        entete = "pseudo".center(10) + " | " + "mdp".center(10) + " | " +\
        "statut".center(10) + " | " + "état".center(10)
        entete_complet = "| " + entete.center(49) + " |"
        contenu = [horiz,titre,horiz,'',horiz,entete_complet,horiz]
        for util in self.dicoUtilisateurs.values():
            chaine = util.getPseudo().center(10) + " | " +\
            util.getMdp().center(10) + " | " +\
            util.getStatut().center(10) + " | " + util.getEtat().center(10)
            contenu.append("| " + chaine + " |")
            contenu.append(horiz)
        return '\n'.join(contenu)
        
     
#Ajout d'utilisateurs
if __name__=="__main__":
    contrib1=Contributeur("cont1","pass1","C","V")
    contrib2=Contributeur("cont2","pass2","C","N")
    contrib3=Contributeur("cont3","pass3","C","N")
    admin=Administrateur("cont4","pass4","A","N")
    base=BaseUtilisateurs()
    base.ajouterContributeur(contrib1)
    base.ajouterContributeur(contrib2)
    base.ajouterContributeur(contrib3)
    base.ajouterContributeur(admin)
    
#Test des fonctionnalités
    base.supprimerContributeur("cont1","admin1")

    base.modifierEtats("cont2","N")

    base.recupererEtatsNonValides()

    base.validerCompte(["cont1","cont2"],admin)
    
    print(base)
    print(contrib1)
