# -*- coding: utf-8 -*-
"""
Created on Fri May 17 19:41:35 2019

@author: id1229
"""


if __name__ == '__main__':
    import os
    import sys
    import collections
    import numpy as np
    import matplotlib
    # matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    # from pyautogui import typewrite

    chem_pgms = os.getcwd()+'\\pgms'
    chem_data = os.getcwd()+'\\data'
    sys.path.append(chem_pgms)
    sys.path.append(chem_data)

    from class_appli import *
    from class_ecran import *
    from class_utilisateurs import *
    from class_produit import *
    
    #os.system("pause")

    #instance de l'application
    appli = Application()
    #instances des menus
    appli.chargementMenus('\\menus.csv')
    #instances des prompts
    appli.chargementPrompts('\\prompts.csv')
    #instances des modifs
    appli.chargementModifs('\\modifs.csv')
    #les utilisateurs
    appli.chargementUtilisateurs('\\base_utilisateurs.csv')
    #les produits
    appli.chargementProduits('\\open_food_facts.csv')

#    choix = appli.accueil()
    
#    compteur = 0
    for cle in appli.base_produits.base.keys():
#        compteur += 1
        print(cle) if cle == '3560070531714' else 2+2
#        if compteur % 10 == 0:
#            os.system("pause")

#    while( choix.upper() != 'Q'):
#        # print(appli.produit_choisi)
#        #print(type(choix))
#        #print(choix)
#        choix = appli.afficherEcran(choix)
#        
#        # print(appli.produit_choisi)
#        
#    appli.quitter()