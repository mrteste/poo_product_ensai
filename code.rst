Documentation automatique
=========================

Classes Produit et BaseProduits
-------------------------------

.. automodule:: pgms.class_produit
   :members:   



Classes Personne, Contributeur et Administrateur
------------------------------------------------

.. automodule:: pgms.class_utilisateurs
   :members:



Classes Ecran, Menu, Prompt et Modif
------------------------------------

.. automodule:: pgms.class_ecran
   :members:


Classe Application
------------------

.. automodule:: pgms.class_appli
   :members: