Contexte de développement
=========================

Le projet de développement de l'application est sous 
.. _gitlab: https://gitlab.com/mrteste/poo_product_ensai.git

Le code a été développé avec Python 3.5 et Python 3.6.

Les modules Python importés et nécessaires au fonctionnement de l'application sont :
	- os
	- sys
	- time
	- collections
	- matplotlib
	- numpy
	- pyautogui (uniquement pour la version 3.6 - voir ci-dessous les contextes de développement)


Un développement, deux contextes
--------------------------------

De manière peu professionnelle, l'application a été développée dans deux contextes
de développement différents :

* le contexte que nous appellerons "Ensai" :
	- Python 3.5
	- Impossibilité d'installer ou d'ajouter de nouveaux modules
	- Console Windows qui ne s'affiche pas en plein écran
	- Connexion avec gitlab impossible

* le contexte que nous appellerons "Perso" : 
	- Python 3.6 sous Windows 10
	- Console Windows permettant l'affichage plein écran
	- Installation à volonté de modules supplémentaires
	- Connexion avec gitlab possible
	
Conséquences/Problèmes rencontrés
---------------------------------

* Le développement sous Python 3.5 et 3.6 en parallèle n'a pas eu de conséquences
trop dommageables. Le seul point problématique est la gestion de l'ordre des clés
des dictionnaires. En effet, jusqu'à la version 3.5 incluse, l'ordre des clés ne
repose pas sur l'ordre d'entrée des éléments dans le dictionnaire ou sur l'ordre
alphanumérique des clés. Or, les menus de notre application sont enregistrés sous la forme
de dictionnaires dont les clés sont les numéros - de type str - de chaque intitulé.
Ainsi, l'affichage des menus dans l'application sous Python 3.5 montrait des intitulés
non rangés comme attendu, le choix 3 pouvant venir avant le choix 2, etc.
Or, ce problème ne s'est pas rencontré sous Python 3.6, car, depuis cette version,
les clés des dictionnaires sont ordonnées. Pour résoudre le problème sous Python 3.5,
nous avons utilisé le module `collections` qui met à disposition la structure d'objet
`OrderedDict` qui gère ce problème. Nous avons appliqué cette solution également
sur la version Python 3.6.

* Le plus gros inconvénient du travail dans le contexte 'Ensai', c'est l'impossibilité
d'installer des modules. Ceci touche notre application pour la gestion des modifications
des produits de la base (`class_ecran.Modfi`). Dans le contexte 'Perso', nous avons
pu installer le module `pyautogui` dont la fonction `typewrite` permet de mettre 
à disposition du texte modifiable par l'utilisateur dans un `input`. Ainsi, un
utilisateur peut reprendre l'ancienne valeur pour la corriger sans devoir tout réécrire.
Or l'impossibilité d'installer des modules dans le contexte 'Ensai' nous a imposé
de renoncer à cette fonctionnalité pourla version Python 3.5.


Avertissement dans le cadre d'un lancement de l'application dans le contexte "Ensai"
------------------------------------------------------------------------------------

Pour une utilisation optimale de l'application, nous conseillons de la lancer depuis
l'environnement de développement **Spyder**. Il suffit pour cela:

* d'ouvrir une console IPython sous Spyder
* de taper la ligne suivante : 
**cd O:/Annee1/info/Groupe38/poo_product_ensai_py35**

* d'ouvrir le fichier **lancement_appli.py** sous ce même dossier
* d'exécuter le code

L'interaction avec les menus se fera dès lors dans la console IPython de Spyder.

Le lancement de l'application depuis la Console Windows des ordinateurs de l'Ensai
comporte quelque bugs que nous nous n'expliquons pas.



